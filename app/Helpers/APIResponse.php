<?php

namespace App\Helpers;

class APIResponse
{
    public static function successResponse($message = null, $data = null, $code = 200)
    {
        return [
            'message' => $message,
            'data' => $data,
            'statusCode' => $code,
        ];
    }

    public static function errorResponse($message, $code = 404, $data = null)
    {
        return [
            'success' => false,
            'data' => $data,
            'statusCode' => $code,
            'message' => $message,
        ];
    }
}
