<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\APIResponse;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests\StoreAppointmentFormRequest;
use App\Models\Appointment;
use Carbon\Carbon;

class AppointmentController extends Controller
{
  /**
   * Display a Listing of Appointments
   *
   * @return \Illuminate\Http\Response
   */

  public function getListAppointments()
  {
    $appointments = Appointment::paginate(10);
    return APIResponse::successResponse('', $appointments);
  }

  /**
   * Getting a single appointment identified by appointment_identifer
   */

  public function getAppointment($appointment_identifier)
  {
    $appointment = Appointment::where(
      'appointment_identifier',
      $appointment_identifier
    )->first();

    if (!$appointment) {
      $response = APIResponse::errorResponse('Appointment not found', 404);
      return response()->json($response);
    }

    return response()->json(
      APIResponse::successResponse(
        '',
        $appointment
      )
    );
  }

  /**
   * Creating a new Appointment
   */

  public function createAppointment(StoreAppointmentFormRequest $request)
  {
    // bin2hex(random_bytes(6)) generate a random binary string with 6 length that converts to hex. ej : 21678769

    $appointment = Appointment::create([
      'appointment_date' => Carbon::createFromFormat(
        'Y-m-d H:i',
        implode(" ", $request->validated())
      )->toDateTimeString(),
      'appointment_identifier' => bin2hex(random_bytes(6)),
      "doctor_id" => $request->doctor_id,
      "patient_id" => $request->patient_id,
    ]);

    return response()->json(
      APIResponse::successResponse(
        'The appointment has been created succesfully',
        $appointment,
        201
      ),
      201
    );
  }

  /**
   * Updating Appointment
   */

  public function updateAppointment(
    StoreAppointmentFormRequest $request,
    $appointment_identifier
  ) {
    $appointment = Appointment::where(
      'appointment_identifier',
      $appointment_identifier
    )->update($request->validated());

    if (!$appointment) {
      return APIResponse::errorResponse('The appointment does not exists');
    }

    return response()->json(
      APIResponse::successResponse(
        'The appointment has been updated succesfully',
        '',
        204
      ),
      204
    );
  }

  /**
   * Deleting Appointment
   */

  public function deleteAppointment($appointment_identifier)
  {
    $appointment = Appointment::where(
      'appointment_identifier',
      $appointment_identifier
    )->delete();

    if (!$appointment) {
      return APIResponse::errorResponse('The appointment does not exists');
    }

    return response()->json(
      APIResponse::successResponse(
        'The appointment has been deleted succesfully',
        '',
        204
      ),
      204
    );
  }
}
