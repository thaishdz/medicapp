<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\APIResponse;

use App\Models\User;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Appointment;

class DoctorController extends Controller
{
  /**
   * Display a Listing of Doctors
   *
   * @return \Illuminate\Http\Response
   */

  public function getDoctorsList()
  {
    $doctors = User::where('role', 'doctor')->paginate(10);
    return response()->json(APIResponse::successResponse('', $doctors));
  }

  /**
   * Getting a single patient identified by Security Social Number
   */

  public function getDoctor($SS_number)
  {
    $doctor = User::where('role', 'doctor')
      ->where('SS_number', $SS_number)
      ->first();

    if (!$doctor) {
      return response()->json(
        APIResponse::errorResponse('Doctor not found'),
        404
      );
    }

    return response()->json(APIResponse::successResponse('', $doctor));
  }

  /**
   * Getting all appointments that owns the doctor
   * TODO: To improve this function searching how to optimizase queries
   * Subqueries?
   */

  public function getAppointmentsBySS($SS_number)
  {
    // Check if the user like a doctor exists
    $doctor = Doctor::join(
      "medicAPP_users",
      "medicAPP_doctors.user_id",
      "=",
      "medicAPP_users.id_user"
    )
      ->where("SS_number", $SS_number)
      ->get();

    if (!$doctor) {
      return response()->json(
        APIResponse::errorResponse('Doctor not found'),
        404
      );
    }

    // Getting doctor's appointments

    $appointments = $doctor[0]->appointments;
    if ($appointments->isEmpty()) {
      return response()->json(
        APIResponse::errorResponse('Appointments not found','',404),
        404
      );
    }

    // We need patient model because has patient firstname and lastname
    $patient = Patient::join(
      "medicAPP_users",
      "medicAPP_patients.user_id",
      "=",
      "medicAPP_users.id_user"
    )
      ->where("id_patient", $appointments[0]->patient_id) // get patient_id from appointments function
      ->get();

    $appointments->put(
      'doctor_name',
      $doctor[0]->firstname . " " . $doctor[0]->lastname
    );
    $appointments->put(
      'patient_name',
      $patient[0]->firstname . " " . $patient[0]->lastname
    );

    return response()->json(
      APIResponse::successResponse('', $appointments),
      200
    );
  }
}
