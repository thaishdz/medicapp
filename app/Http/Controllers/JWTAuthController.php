<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Nurse;

use App\Helpers\APIResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Validation\UserValidator;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\LoginRequest;

use App\Http\Controllers\Controller;

class JWTAuthController extends Controller
{
  /**
   * Get the login username to be used by the controller.
   *
   * @return string
   */
  public function username()
  {
    return "username";
  }
  /**
   * Create a new AuthController instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth:api', ['except' => ['login', 'register']]);
  }

  /**
   * Register a User.
   *
   * @return \Illuminate\Http\JsonResponse
   */

  public function register(StoreUserRequest $request)
  {
    $user = User::create($request->validated());

    if ($user->role === 'patient') {
      Patient::create(['user_id' => $user->id_user]);
    } elseif ($user->role === 'doctor') {
      Doctor::create(['specialty' => null, 'user_id' => $user->id_user]);
    } else {
      Nurse::create(['user_id' => $user->id_user]);
    }

    return response()->json(
      APIResponse::successResponse('Successfully registered', '', 201),
      201
    );
  }

  /**
   * Get a JWT via given credentials.
   *
   * @return token
   */

  public function login(LoginRequest $request)
  {
    // Checking if the user is registered

    $token = Auth::attempt($request->validated());

    if (!$token) {
      return response()->json(['error' => 'Unauthorized'], 401);
    }

    return $this->createNewToken($token);
  }

  /**
   * Get the authenticated User.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function profile()
  {
    return response()->json(auth()->user());
  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout()
  {
    auth()->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh()
  {
    return $this->createNewToken(auth()->refresh());
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function createNewToken($token)
  {
    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' =>
        auth()
          ->factory()
          ->getTTL() * 60,
    ]);
  }
}
