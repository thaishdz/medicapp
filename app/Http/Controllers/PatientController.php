<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;

use App\Rules\NIF;
use Illuminate\Support\Facades\Validator;

use App\Helpers\APIResponse;
use App\Models\User;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Appointment;

class PatientController extends Controller
{
  /**
   * Display a Listing of Patients
   *
   * @return \Illuminate\Http\Response
   */

  public function getPatientsList()
  {
    $patients = User::where('role', 'patient')->paginate(10);
    return response()->json(APIResponse::successResponse('',$patients));
  }

  /**
   * Getting a single patient identified by Security Social Number
   */

  public function getPatient($SS_number)
  {
    $patient = User::with('patient')
      ->where('SS_number', $SS_number)
      ->first();

    if (!$patient) {
      return response()->json(
        APIResponse::errorResponse('Patient not found'),
        404
      );
    }

    return response()->json(APIResponse::successResponse('',$patient));
  }

  /**
   * Getting all patient's appointments
   * TODO: To improve this function searching how to optimizase queries
   */

  public function getAppointmentsBySSNumber($SS_number)
  {
    $patient = Patient::join(
      "medicAPP_users",
      "medicAPP_patients.user_id",
      "=",
      "medicAPP_users.id_user"
    )
      ->where("ss_number", $SS_number)
      ->get();

    if (!$patient) {
      return response()->json(
        APIResponse::errorResponse('Patient not found'),
        404
      );
    }

    $appointments = $patient[0]->appointments;
    if ($appointments->isEmpty()) { // check collection is empty or Not
      return response()->json(
        APIResponse::errorResponse('Appointments not found'),
        404
      );
    }

    // getting doctor and patient names
    $doctor = Doctor::join(
      "medicAPP_users",
      "medicAPP_doctors.user_id",
      "=",
      "medicAPP_users.id_user"
    )
      ->where("id_doctor", $appointments[0]->doctor_id) // get patient_id from appointments function
      ->get();

    $appointments->put(
      'doctor_name',
      $doctor[0]->firstname . " " . $doctor[0]->lastname
    );
    $appointments->put(
      'patient_name',
      $patient[0]->firstname . " " . $patient[0]->lastname
    );

    return response()->json(
      APIResponse::successResponse('', $appointments),
      200
    );
  }
}
