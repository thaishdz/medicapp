<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\APIResponse;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserFormRequest;

class UserController extends Controller
{
  /**
   * Display a Listing of Users
   *
   * @return \Illuminate\Http\Response
   */

  public function getUsersList()
  {
    $users = User::paginate(10);
    return response()->json(APIResponse::successResponse('', $users), 200);
  }

  /**
   * Getting a single user identified by SS_number
   * @return App\Helpers\APIResponse
   */

  public function getUser($SS_number)
  {
    $user = User::where('SS_number', $SS_number)->first();

    if (!$user) {
      return response()->json(
        APIResponse::errorResponse('User not found', '', 404),
        404
      );
    }

    return response()->json(APIResponse::successResponse('', $user, 200), 200);
  }

  /**
   * Updating User
   * @return App\Helpers\APIResponse
   */

  public function updateUser(UpdateUserFormRequest $request, $SS_number)
  {
    $user = User::where('SS_number', $SS_number)->update($request->validated());

    return response()->json(
      APIResponse::successResponse('Successfully updated', $user, 204),
      204
    );
  }
}
