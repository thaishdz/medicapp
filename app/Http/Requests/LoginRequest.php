<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\User;
use App\Rules\SS_number;
use Illuminate\Validation\Rule;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'ss_number' => ['required', 'string', new SS_number()],
      'password' => 'required|string|min:6',
    ];
  }

  /**
   * Failed validation disable redirect
   *
   * @param Validator $validator
   */
  protected function failedValidation(Validator $validator)
  {
    throw new HttpResponseException(
      response()->json($validator->errors(), 422)
    );
  }
}
