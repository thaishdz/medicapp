<?php

namespace App\Http\Requests;

use App\Rules\NIF;
use App\Models\User;
use App\Rules\SS_number;
use App\Helpers\APIResponse;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreUserRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'firstname' => 'required|min:5|max:20',
      'lastname' => 'required|min:5|max:50',
      'email' => 'required',
      'nif' => ['required', 'string', 'unique:medicAPP_users,nif', new NIF()],
      'ss_number' => [
        'required',
        'string',
        'unique:medicAPP_users,ss_number',
        new SS_number(),
      ],
      'role' => 'required|in:patient,doctor,nurse',
      'password' => 'required|string|min:6',
    ];
  }

  public function messages()
  {
    return [
      'unique' => 'The :attribute already exists',
    ];
  }

  /**
   * Failed validation disable redirect
   *
   * @param Validator $validator
   */
  protected function failedValidation(Validator $validator)
  {
    throw new HttpResponseException(
      response()->json($validator->errors(), 422)
    );
  }
}
