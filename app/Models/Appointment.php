<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array - Appointment::create([]);
   */
  protected $fillable = [
    'appointment_date',
    'appointment_identifier',
    'patient_id',
    'doctor_id',
    'nurse_id',
  ];

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'medicAPP_appointments';

  /**
   * The primary key associated with the table.
   * @var string
   */
  protected $primaryKey = 'id_appointment';

  /**
   * The attributes that should be hidden for arrays and JSON Response
   *
   * @var array
   */
  protected $hidden = ['patient_id', 'doctor_id', 'nurse_id'];

  /**
   * Get the appointments that owns the patient.
   */

  public function patient()
  {
    return $this->belongsTo('App\Models\Patient');
  }

  /**
   * Get the appointments that owns the health professional.
   */
}
