<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
  protected $fillable = ['specialty', 'user_id'];

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'medicAPP_doctors';

  /**
   * The primary key associated with the table.
   * @var string
   */
  protected $primaryKey = 'id_doctor';

  /**
   * Relation 1:n between Health Professional and Appointments
   */

  public function appointments()
  {
    return $this->hasMany(Appointment::class, 'doctor_id');
  }

  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }
}
