<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nurse extends Model
{
  protected $fillable = ['user_id'];

  protected $table = 'medicAPP_nurses';

  protected $primaryKey = 'id_nurse';

  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }
}
