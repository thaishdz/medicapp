<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
  protected $fillable = ['user_id'];

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'medicAPP_patients';

  /**
   * The primary key associated with the table.
   * @var string
   */
  protected $primaryKey = 'id_patient';

  /**
   * Relation 1:n between Patient and Appointments
   */

  public function appointments()
  {
    return $this->hasMany('App\Models\Appointment', 'patient_id');
  }

  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }
}
