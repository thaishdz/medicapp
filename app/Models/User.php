<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'firstname',
    'lastname',
    'email',
    'nif',
    'ss_number',
    'role',
    'password',
  ];

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'medicAPP_users';

  /**
   * The primary key associated with the table.
   * @var string
   */
  protected $primaryKey = 'id_user';

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['password', 'remember_token', 'id_user'];

  public function patient()
  {
    return $this->hasOne('App\Models\Patient', 'user_id');
  }

  public function doctor()
  {
    return $this->hasOne('App\Models\Doctor', 'user_id');
  }

  public function nurse()
  {
    return $this->hasOne('App\Models\Nurse', 'user_id');
  }

  /**
   * Get the identifier that will be stored in the subject claim of the JWT.
   *
   * @return mixed
   */
  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims()
  {
    return [];
  }

  public function setPasswordAttribute($value)
  {
    $this->attributes['password'] = bcrypt($value);
  }
}
