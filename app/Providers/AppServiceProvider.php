<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    if (config('app.debug') != false) {
      DB::listen(function ($query) {
        Log::debug('[' . $query->time . ' ms] ' . $query->sql);
        if (count($query->bindings) > 0) {
          Log::debug('- Params -> ' . print_r($query->bindings, true));
        }
      });
    }
  }
}
