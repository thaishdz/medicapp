<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NIF implements Rule
{

    /**
     * Determine if the validation nif passes.
     *
     * @param string $attribute "nif"
     * @param  string  $nif "68989311Y"
     * @return bool
     */
    public function passes($attribute, $nif)
    {
        $regex = '/^[0-9]{8}[A-Z]$/i'; // checking format of nif

        $letters = 'TRWAGMYFPDXBNJZSQVHLCKE';

        if (preg_match($regex, $nif)) {

            // checking if the letter is correct
            return $letters[(substr($nif, 0, 8) % 23)] === $nif[8];
        }

        return false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is not valid';
    }
}
