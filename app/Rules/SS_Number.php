<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * @name Validación de código de la Seguridad Social
 * @copyright (c)2015 Intervia IT
 * @link http://intervia.com/doc/validar-numeros-de-la-seguridad-social/
 * @license MIT http://opensource.org/licenses/MIT
 */

class SS_number implements Rule
{
  /**
   * Create a new rule instance.
   *
   * @return void
   */
  public function __construct()
  {
  }

  /**
   * Determine if the validation rule passes.
   *
   * @param  string  $attribute
   * @param  mixed  $ss_number
   * @return bool
   */
  public function passes($attribute, $ss_number)
  {
    /**
     * If the Security Social Number exists and it has 12 digits then break down in
     * province_code
     * affiliate_number
     * control_number
     * for validating
     */
    if (strlen($ss_number) != 12) {
      return false;
    } else {
      $province_code = substr($ss_number, 0, 2);
      $affiliate_number = (int) substr($ss_number, 2, 8);
      $control_number = substr($ss_number, 10, 2);
    }

    /**
     * If the affiliate number , is less than 10 million, the validation code must be the rest of divide by 97 the result of this operation
     */
    if ($affiliate_number < 10000000) {
      $result = $affiliate_number + $province_code * 10000000;
    } else {
      $result = $province_code . $affiliate_number; // assigning concatenation between $province_code and $affiliate_number
    }

    // Getting validation code
    $validation_code = $result % 97;

    // Checking validation code with control_number
    if ($validation_code != $control_number) {
      return false;
    }

    return true;
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message()
  {
    return 'The Security Social Number is not valid';
  }
}
