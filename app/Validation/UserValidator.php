<?php

namespace App\Validation;

use Illuminate\Validation\Rule;
use App\Rules\NIF;
use App\Rules\SS_Number;

use Illuminate\Http\Request;
use App\Models\User;

use Validator;

class UserValidator
{
  public static function validationRequest(Request $request)
  {
    $messages = [
      'unique' => 'The :attribute already exists',
    ];

    return $validator = Validator::make(
      $request->all(),
      [
        'firstname' => 'required|min:5|max:20',
        'lastname' => 'required|min:5|max:50',
        'email' => 'required',
        'nif' => ['required', 'string', 'unique:medicAPP_users,nif', new NIF()],
        'ss_number' => [
          'required',
          'string',
          'unique:medicAPP_users,ss_number',
          new SS_number(),
        ],
        'role' => 'required',
        'password' => 'required|string|min:6',
      ],
      $messages
    );
  }

  public static function validateLogin(Request $request)
  {
    return $validator = Validator::make($request->all(), [
      'ss_number' => ['required', 'string', new SS_number()],
      'password' => 'required|string|min:6',
    ]);
  }
}
