<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Appointment;
use Faker\Generator as Faker;

$factory->define(Appointment::class, function (Faker $faker) {
  return [
    'appointment_date' => $faker->datetimeThisYear,
    'appointment_identifier' => $faker->ean8,
    'patient_id' => 1,
    'doctor_id' => 1
  ];
});
