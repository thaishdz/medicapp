<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
  return [
    'firstname' => $faker->firstName,
    'lastname' => $faker->lastName,
    'email' => $faker->email,
    'nif' => '42011484Y',
    'ss_number' => '290774437461',
    'role' => 'patient',
    'remember_token' => null,
    'password' => Str::random(10),
  ];
});
