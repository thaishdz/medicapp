<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('medicAPP_users', function (Blueprint $table) {
      $table->id('id_user');
      $table->string('firstname', 400);
      $table->string('lastname', 400);
      $table->string('email');
      $table->string('nif', 9);
      $table->string('ss_number');
      $table->enum('role', ['patient', 'doctor', 'nurse']);
      $table->string('password');

      $table->rememberToken();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('medicAPP_users');
  }
}
