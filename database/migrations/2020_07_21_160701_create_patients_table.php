<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('medicAPP_patients', function (Blueprint $table) {
      $table->id('id_patient');
      $table->unsignedBigInteger('user_id');
      $table->timestamps();

      $table
        ->foreign('user_id')
        ->references('id_user')
        ->on('medicAPP_users')
        ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('medicAPP_patients');
  }
}
