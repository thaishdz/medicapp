<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNursesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('medicAPP_nurses', function (Blueprint $table) {
      $table->id('id_nurse');
      $table->unsignedBigInteger('user_id');
      $table->timestamps();

      $table
        ->foreign('user_id')
        ->references('id_user')
        ->on('medicAPP_users')
        ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('medicAPP_nurses');
  }
}
