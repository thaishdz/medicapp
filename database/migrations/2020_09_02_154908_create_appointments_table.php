<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('medicAPP_appointments', function (Blueprint $table) {
      $table->id('id_appointment');
      $table->datetime('appointment_date');
      $table->string('appointment_identifier');

      $table->unsignedBigInteger('patient_id');
      $table->unsignedBigInteger('doctor_id');
      $table->unsignedBigInteger('nurse_id')->nullable();

      $table->timestamps();

      $table->index('patient_id');
      $table->index('doctor_id');
      $table->index('nurse_id');

      // Creating a foreign keys that references to the table medicAPP_ with primary key id_

      $table
        ->foreign('patient_id')
        ->references('id_patient')
        ->on('medicAPP_patients')
        ->onDelete('cascade');
      $table
        ->foreign('doctor_id')
        ->references('id_doctor')
        ->on('medicAPP_doctors')
        ->onDelete('cascade');
      $table
        ->foreign('nurse_id')
        ->references('id_nurse')
        ->on('medicAPP_nurses')
        ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('medicAPP_appointments');
  }
}
