<?php

use Illuminate\Database\Seeder;

use App\Appointment;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // using the factory defined in AppointmentFactory

        //factory(Appointment::class, 10)->create(); // Create 10 Appointments instances

        DB::table('medicAPP_appointments')->insert([
            "appointment_date" => date('Y-m-d H:i:s'),
            "appointment_identifier" => "1111111",
            "patient_id" => 1,
            "doctor_id" => 3
        ]);
    }
}
