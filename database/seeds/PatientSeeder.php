<?php

use App\Patient;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // using the factory defined in PatientFactory

    //factory(Patient::class, 10)->create(); // Create 10 Patient instances


    // Create 10 Patients instances with unique user_id

    for ($i = 0; $i < 10; $i += 1) {
      $user = factory(App\Models\User::class)->create();
      Patient::create([
        'user_id' => $user->id_user,
      ]);
    }
  }
}
