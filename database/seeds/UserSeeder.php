<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // using the factory defined in UserFactory

    //factory(User::class, 10)->create();

    DB::table('medicAPP_users')->insert([
      'firstname' => 'Eufemio',
      'lastname' => 'Valdomero Gutierrez',
      'email' => 'eufe@gmail.com',
      'nif' => '10735673D',
      'ss_number' => '430139606713',
      'role' => 'patient',
      'password' => 'fire123'
    ]);
  }
}
