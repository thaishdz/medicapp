<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Users

Route::get('users', 'UserController@getUsersList');
Route::get('users/{ss_number}', 'UserController@getUser');
Route::post('users', 'UserController@createUser');
Route::put('users/{ss_number}', 'UserController@updateUser');
Route::delete('users/{ss_number}', 'UserController@deleteUser');

// Patients

Route::get('patients', 'PatientController@getPatientsList');
Route::get('patients/{ss_number}', 'PatientController@getPatient');
Route::get(
  'patients/{ss_number}/appointments',
  'PatientController@getAppointmentsBySSNumber'
);

// Doctors

Route::get('doctors', 'DoctorController@getDoctorsList');
Route::get('doctors/{ss_number}', 'DoctorController@getDoctor');
Route::get(
  'doctors/{ss_number}/appointments',
  'DoctorController@getAppointmentsBySS'
);

//Appointments

Route::get('appointments', 'AppointmentController@getListAppointments');
Route::get(
  'appointments/{appointment_identifier}',
  'AppointmentController@getAppointment'
);
Route::post('appointments', 'AppointmentController@createAppointment');
Route::put(
  'appointments/{appointment_identifier}',
  'AppointmentController@updateAppointment'
);
Route::delete(
  'appointments/{appointment_identifier}',
  'AppointmentController@deleteAppointment'
);

// Authentication

Route::group(
  [
    'middleware' => 'api',
    'prefix' => 'auth',
  ],
  function ($router) {
    Route::post('register', 'JWTAuthController@register');
    Route::post('login', 'JWTAuthController@login');
    Route::post('logout', 'JWTAuthController@logout');
    Route::post('refresh', 'JWTAuthController@refresh');
    Route::get('profile', 'JWTAuthController@profile');
  }
);
