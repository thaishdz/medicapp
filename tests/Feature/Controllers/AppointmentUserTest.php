<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Doctor;
use App\Models\Patient;
use App\Models\Appointment;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppointmentUserTest extends TestCase
{
  // use RefreshDatabase;
  /**
   * A basic feature test example.
   *
   * @return void
   */
  public function test_a_patient_can_create_an_appointment()
  {
    $user1 = factory(User::class)->create();
    $user2 = factory(User::class)->create([
      'nif' => '93313557G',
      'ss_number' => '304280560794',
      'role' => 'doctor',
    ]);

    $patient = Patient::create([
      'id_patient' => 1,
      'user_id' => 1,
    ]);

    $doctor = Doctor::create([
      'id_doctor' => 1,
      'user_id' => 2,
    ]);

    // $this->assertEquals(2, User::all()->count());
    // $this->assertEquals(1, Patient::all()->count());
    // $this->assertEquals(1, Doctor::all()->count());

    $appointment = [
      'appointment_date' => '2020-11-10 12:30',
      'appointment_identifier' => 'fa356909f685',
      'patient_id' => 1,
      'doctor_id' => 1,
    ];

    $response = $this->postJson('api/appointments', $appointment);
    $response->assertCreated();
  }

  public function test_a_doctor_can_see_his_appointments()
  {
    $user = factory(User::class)->create([
      'nif' => '93313557G',
      'ss_number' => '304280560794',
      'role' => 'doctor',
    ]);

    $response = $this->get('api/${$user->ss_number}/appointments');
    dd($response);

    $response->assertStatus(200);
  }
}
