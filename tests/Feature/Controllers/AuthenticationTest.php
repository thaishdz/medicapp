<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;

use JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthenticationTest extends TestCase
{
  use RefreshDatabase;

  /**
   * Test if user can register trough internal api.
   */
  public function test_a_user_can_register()
  {
    //User's data

    $userData = [
      'firstname' => 'Eufemio',
      'lastname' => 'Valdomero Gutierrez',
      'email' => 'eufe@gmail.com',
      'nif' => '10735673D',
      'ss_number' => '430139606713',
      'role' => 'patient',
      'password' => 'fire123',
    ];

    //Send post request
    $response = $this->postJson('api/auth/register', $userData);

    // Assert that the response has a 201 status code
    $response->assertCreated();
  }

  /**
   * Test if user can login trough internal api.
   */

  public function test_a_user_can_log_in()
  {
    $user = factory(User::class)->create();

    // the user created should be logged with token
    $response = $this->actingAs($user, 'api')->postJson('api/auth/login', [
      'ss_number' => $user->ss_number,
      'password' => $user->password,
    ]);

    $response->assertJsonStructure([
      'access_token',
      'token_type',
      'expires_in',
    ]); // must return this json structure
  }
}
